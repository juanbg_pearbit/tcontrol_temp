package io.pearbit.tcontrol.dao;

import java.util.List;
import java.util.Optional;

import io.pearbit.tcontrol.models.User;

/**
 * To be implemented by any class which want to figure out like a DAO for User entity
 *
 * @author JuanBG
 *
 */
public interface UserDao{
	
	/**
	 * Save a new user.
	 * @param user to be saved
	 */
	public void save(User user);
	
	/**
	 * Made a search by user name field 
	 * @param username 
	 * @return it is optional 'cause it cannot exist
	 */
	public Optional<User> findByUsername(String username);
	
	/**
	 * Return all users
	 * @return
	 */
	public List<User> findAll();
	
	/**
	 * Modify a user in base a concrete user object
	 * @param user
	 * @return it can be null for this it is optional
	 */
	public Optional<User> modify(User user);
	/**
	 * delete
	 * @param user
	 */
	public void delete(User user);
	
	/**
	 * This method format an user object in a secure object to be persisted. The most important aspect is that it encrypt password 
	 * @param user
	 */
	public void userSystemConfig(User user);
}
