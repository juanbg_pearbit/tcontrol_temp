package io.pearbit.tcontrol.dao.sql;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import io.pearbit.tcontrol.dao.RolDao;
import io.pearbit.tcontrol.models.Rol;

/**
 * Rol repository
 * @author JuanBG
 *
 */
@Repository
public class RolDaoImpl implements RolDao{
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public Rol findByName(String name) {
		Session session = sessionFactory.openSession();
		Rol rol = null;
		try {
			session.beginTransaction();
			Query query = session.createQuery("from Rol where name = :r_name"); // HQL que hace referencia a SELECT * from users 
			query.setString("r_name", name); // se le manda el parametro u_name
			rol = (Rol) query.uniqueResult();
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			//DEBUG
		} finally {
			session.close();
		}
		return rol;
	}

	@Override
	public void save(Rol rol) {
		Session session = sessionFactory.openSession(); //abrir sesi�n
		try {
			session.beginTransaction(); // Comenzar transacci�n, todas las operaciones que tocan la base de datos, se les conoce como transacciones.
			session.save(rol); //una vez abierta la sesi�n le decimos a hibernate que guarde en el �rea de espera el recurso a persistir 
			session.getTransaction().commit();//le damos la orden a la transacci�n de realizar el commit a la base de datos 
			
		} catch (Exception e) {
			session.getTransaction().rollback(); //si algo falla hacemos un rollback para deshacer los posibles cambios
			e.printStackTrace();
			//DEBUG
		} finally {
			session.close();//cerramos sesi�n

		}
	}

}
