package io.pearbit.tcontrol.dao.sql;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import io.pearbit.tcontrol.dao.RolDao;
import io.pearbit.tcontrol.dao.UserDao;
import io.pearbit.tcontrol.models.User;

@SuppressWarnings("deprecation")
@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	SessionFactory sessionFactory;
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private RolDao rolRepository;

	@Override
	public void save(User user) {
		Session session = sessionFactory.openSession(); // abrir sesi�n
			session.beginTransaction(); // Comenzar transacci�n, todas las operaciones que tocan la base de datos, se
										// les conoce como transacciones.
			session.save(user); // una vez abierta la sesi�n le decimos a hibernate que guarde en el �rea de
								// espera el recurso a persistir
			session.getTransaction().commit();// le damos la orden a la transacci�n de realizar el commit a la base de
												// datos

	}

	@Override
	public Optional<User> findByUsername(String username) {
		Session session = sessionFactory.openSession();
			session.beginTransaction();
			User user;
			Query query = session.createQuery("from User where username = :u_name"); // HQL que hace referencia a SELECT
																						// * from users
			query.setString("u_name", username); // se le manda el parametro u_name
			user = (User) query.uniqueResult();
			session.getTransaction().commit();
			return Optional.ofNullable(user);
		
	}

	@Override
	public List<User> findAll() {
		Session session = sessionFactory.openSession();
			session.beginTransaction();
			Query query = session.createQuery("from User"); // HQL que hace referencia a un SELECT * FROM users, s�lo
															// que esta hace una referencia al Objeto mapeado en el
															// hibernate.cgf.xml.
			List<User> users = query.list(); // obtenemos una lista
			session.getTransaction().commit();
			return users;
		
	}

	@Override
	public Optional<User> modify(User user) {
		Session session = sessionFactory.openSession();
			session.beginTransaction(); // comenzamos transacci�n
			User n_user = session.get(User.class, user.getUsername()); // Conseguimos la referencia del objeto viejo de
																		// la base de datos
			n_user = user; // al objeto viejo, le sobre escribimos el nuevo objeto, cabe mencionar que con
							// esto no perdemos el id, que es lo que hibernate necesita para persistir el
							// objeto correcto
			session.merge(n_user); // se guardan los cambios en el stage area
			session.getTransaction().commit(); // se hace el commit
			return Optional.ofNullable(n_user);
	}

	@Override
	public void delete(User user) {
		Session session = sessionFactory.openSession();
			session.beginTransaction();
			User userToDelete = session.get(User.class, user.getId()); // obtiene el objeto que ser� eliminado a trav�s
																		// del id
			session.delete(userToDelete); // eliminamos
			session.getTransaction().commit();

	}

	public void userSystemConfig(User user) {
		System.out.println(user);
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setRol(rolRepository.findByName(user.getRol().getName()));
		save(user);


	}

}
