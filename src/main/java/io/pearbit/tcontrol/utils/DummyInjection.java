package io.pearbit.tcontrol.utils;


import java.sql.SQLException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.pearbit.tcontrol.dao.RolDao;
import io.pearbit.tcontrol.models.Rol;

/**
 * Class created for dummy purposes.
 * @author JuanBG
 *
 */
@Component
public class DummyInjection {
	
	@Autowired
	RolDao dao;
	
	/**
	 * it will be executed after the project construction, after {@link SpringWebConfig}.
	 * @return
	 */
	@PostConstruct
	public boolean createRole(){
		
		Rol rol_adm = new Rol(0, "ADMIN");
		Rol rol_usr = new Rol(0, "USER");

			dao.save(rol_adm);
			dao.save(rol_usr);
		
		return true;
	}
	
	
}
