package io.pearbit.tcontrol.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mysql.jdbc.log.LogFactory;

import io.pearbit.tcontrol.dao.UserDao;
import io.pearbit.tcontrol.models.User;
import io.pearbit.tcontrol.models.transport.Message;
import io.pearbit.tcontrol.models.transport.UserBundle;


/**
 * This class perform all task related with User entity.
 * 
 * @author JuanBG
 *
 */

@RestController
@RequestMapping("/user")
public class UserService {
	
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);	
	@Autowired
	private UserDao userDao;
	
	/**
	 * This method register a "Master User" @see PearBit Documentation. This method
	 * will be called by a third actor
	 * 
	 * @param bundle
	 *            come in user from third actor.
	 * 
	 * @return this is an informative answer to invoker. A list of possible error messages
	 * In this case it will only sent error messages, in success case, only will return the HTTP code acording with the case.
	 */
	@PostMapping("/masterUser")
	public ResponseEntity<List<Message>> createMasterUser(@RequestBody UserBundle bundle) {
		
		
		ResponseEntity<List<Message>> messageResponseEntity = null;

		List<Message> messages = new ArrayList<Message>();

		// If user name is empty, will add an Message to List
		if (bundle.getUsername().equals("")) {
			logger.error("User Empty");
			Message message = new Message("ERROR", 
								  "Data not correct", 
								  "Username is empty or not well formed", 
								  new Date());
			messages.add(message);
		}

		// If password is empty, will add an Message to List
		if (bundle.getPassword().equals("")) {
			logger.error("Password Empty");
			Message message = new Message("ERROR",
								  "Data not correct", 
								  "Password is empty or not well formed",
								  new Date());
			messages.add(message);
		}
		

		// If there are message in the list, it means that something was wrong, so will the messageResponseEntity will be formed 
		// to return an 403 Error code and all the messages. 
		// if there aren't messages, the register procedure will continue. and if all is okay will return an HTTP 200 code.
		if (messages.size() == 0) {
			
			logger.error("user good");
			
			userDao.userSystemConfig(new User(bundle.getUsername(), bundle.getPassword()));
			
			messageResponseEntity = ResponseEntity.ok().build();
			
		}else {
			messageResponseEntity = ResponseEntity.badRequest().body(messages);

		}

		return messageResponseEntity;
	}

}
