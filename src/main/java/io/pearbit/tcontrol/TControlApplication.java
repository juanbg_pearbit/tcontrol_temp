package io.pearbit.tcontrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.pearbit.tcontrol.controller.UserService;
/**
 * Start point according with use case design planed by TCONTROL TEAM 
 * are "Two factor validation" but it will be performed by PearBit Master Project Validation System, If all is good, The MPVS 
 * will call the {@link UserService} for createMasterUser() method, 
 * 
 * So the next entry point is irrelevant 'cause, they are services to make registry and start with the management. 
 * Please refer to the above link to see the detail. 
 * 
 * @author JuanBG
 *
 */
@SpringBootApplication
public class TControlApplication {

	
	
	
	public static void main(String[] args) {
		SpringApplication.run(TControlApplication.class, args);
	}
}
