package io.pearbit.tcontrol.config;

import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@ComponentScan(basePackages = {"io.pearbit.tcontrol"})
@PropertySource("application.properties")

public class GeneralConfiguration {
	 
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

	
	
    /**
     * H I B E R N A T E       C O N F I G 
     */
    	
	@Value("${database.driver}") private String dbDriver;
	@Value("${database.url}") private String dbUrl;
	@Value("${database.user}") private String dbUser;
	@Value("${database.password}") private String dbPassword;

	// values for hibernate
	@Value("${hibernate.dialect}") private String hdialect;
	@Value("${hibernate.show_sql}") private String hshowSql;
	@Value("${hibernate.format_sql}") private String hFormatSql;
	@Value("${hibernate.hbm2ddl.auto}") private String ddl_auto;
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	
	@Bean
	@Qualifier(value = "entityManager")
	public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
	    return entityManagerFactory.createEntityManager();
	}
	
	
	@Bean
	public LocalSessionFactoryBean sgetSessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean(); //Create a new object for be returned like in a Singleton 
		sessionFactory.setDataSource(restDataSource()); //Consumes the datasoruce with the database information
		sessionFactory.setPackagesToScan(new String[] { "io.pearbit.tcontrol.models" }); //points out where are the entities  
		sessionFactory.setHibernateProperties(hibernateProperties()); //Are sent the hibernate props. 
		return sessionFactory;
	}
	@Bean("dataSoruce")
	public DataSource restDataSource() {
		BasicDataSource dataSource = new BasicDataSource(); //are sent all props read from the config file
		dataSource.setDriverClassName(dbDriver);
		dataSource.setUrl(dbUrl);
		dataSource.setUsername(dbUser);
		dataSource.setPassword(dbPassword);
		// dataSource.setPassword(env.getProperty("jdbc.pass"));
		
		return dataSource;
	}
	
	Properties hibernateProperties() {
	return new Properties() {
		{
			setProperty("hibernate.dialect", hdialect);
			setProperty("hibernate.format_sql", hFormatSql);
			setProperty("hibernate.show_sql", hshowSql);
			setProperty("hibernate.hbm2ddl.auto", ddl_auto);

		}
	};
}
	
	
}
