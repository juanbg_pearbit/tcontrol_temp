package io.pearbit.tcontrol.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import io.pearbit.tcontrol.controller.UserService;

import javax.persistence.JoinColumn;

@Component("User_E")
@Entity //Declara como entidad
@Table(name = "User") // nombre de la tabla fisica
public class User {
	
	@Id 
	@Column(name = "u_id") //nombre de la columna
	@GeneratedValue //auto_increment
	private int id;
	
    @OneToOne
    @JoinColumn(name = "id")
	private Employee idEmployee;
	
	@Column(name = "u_username")
	private String username;
	
	@Column(name = "u_password")
	private String password;
	
	@Column(name = "u_enable_status")
	private int enable;	
	
	@ManyToOne
	private Rol rol;
	
	/**
	 * Special constructor to create first user, called mainly by PearBit Manage System when it calls the service at 
	 * {@link UserService}
	 * 
	 * @param username
	 * @param password
	 */
	public User(String username, String password) {
		this.id = 0;
		this.username = username;
		this.password = password;
		this.enable = 1;
		this.rol = new Rol(0, "ADMIN");
	}
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}	
		
	public User(int id, String username, String password, int enable, Rol rol) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.enable = enable;
		this.rol = rol;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int isEnable() {
		return enable;
	}
	public void setEnable(int enable) {
		this.enable = enable;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Employee getIdEmployee() {
		return idEmployee;
	}
	public void setIdEmployee(Employee idEmployee) {
		this.idEmployee = idEmployee;
	}
	public int getEnable() {
		return enable;
	}
	public Rol getRol() {
		return rol;
	}
	public void setRol(Rol rol) {
		this.rol = rol;
	}




	@Override
	public String toString() {
		return "User [id=" + id + ", idEmployee=" + idEmployee + ", username=" + username + ", password=" + password
				+ ", enable=" + enable + ", rol=" + rol + "]";
	}

	
	
	
	
	
	
}
