package io.pearbit.tcontrol.models;


import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;
@Component
@Entity //declara como entidad
@Table(name = "Employee") //nombre de tabla física
public class Employee {
	@Id 
	@Column(name = "e_id") //nombre de la columna
	@GeneratedValue //auto_increment
	private int idEmployee;
	
	@ManyToOne
	private Area idArea;
	
    @OneToOne(mappedBy = "idEmployee")
	private User idUser;
	
	@Column(name = "e_number")
	private int noEmployee;
	@Column(name = "e_name")
	private String name;
	@Column(name = "e_second_name")
	private String secondName;
	@Column(name = "e_lastname")
	private String lastName;
	@Temporal(TemporalType.DATE)
	@Column(name = "e_birthday")
	private Date birthday;
	@Column(name = "e_curp")
	private String curp;
	@Column(name = "e_picture")
	private String pPicture;
	@Column(name = "e_finger")
	private Blob eFinger;
	@Column(name = "e_ip")
	private String ip;
	@Column(name = "e_mac")
	private String mac;
	@Column(name = "e_status")
	private int status;
	
	
	
	public int getIdEmployee() {
		return idEmployee;
	}
	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}
	public Area getIdArea() {
		return idArea;
	}
	public void setIdArea(Area idArea) {
		this.idArea = idArea;
	}
	public int getNoEmployee() {
		return noEmployee;
	}
	public void setNoEmployee(int noEmployee) {
		this.noEmployee = noEmployee;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getpPicture() {
		return pPicture;
	}
	public void setpPicture(String pPicture) {
		this.pPicture = pPicture;
	}
	public Blob geteFinger() {
		return eFinger;
	}
	public void seteFinger(Blob eFinger) {
		this.eFinger = eFinger;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
	
	
}
