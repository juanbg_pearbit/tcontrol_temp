package io.pearbit.tcontrol.models.transport;

/**
 * This class represent the next object: 
 * 	 {username: "something", password:"somethingEncrypted"}
 * And is used to transport data in a save package
 * @author JuanBG
 *
 */
public class UserBundle {
	private String username;
	private String password;

	public UserBundle() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public UserBundle(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
}
