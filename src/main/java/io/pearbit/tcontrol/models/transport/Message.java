package io.pearbit.tcontrol.models.transport;

import java.util.Date;

/**
 *  Object used to sent an general answer. 
 *  
 * @author JuanBG
 *
 */
public class Message {
	private Date timestamp;
	private String status;
	private String message;
	private String descrption;
	public Message() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Message(String status, String message, String descrption, Date timestamp) {
		super();
		this.status = status;
		this.message = message;
		this.descrption = descrption;
		this.timestamp = timestamp;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDescrption() {
		return descrption;
	}
	public void setDescrption(String descrption) {
		this.descrption = descrption;
	}
	public Date getDate() {
		return timestamp;
	}
	public void setDate(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	
	
}
