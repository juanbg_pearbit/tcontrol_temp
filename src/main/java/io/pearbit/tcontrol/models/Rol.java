package io.pearbit.tcontrol.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity //declara como entidad
@Table(name = "Rol") //nombre de tabla física
public class Rol {
	
	@Id //id
	@GeneratedValue //auto_increment
	@Column(name = "r_id")
	private int id;
	
	@Column (name = "r_name")
	private String name;
	
    @OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="u_id")
    private List<User> user;
    
    
    
	
	public Rol(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public Rol() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<User> getUser() {
		return user;
	}
	public void setUser(List<User> user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Rol [id=" + id + ", name=" + name + "]";
	}
	
	
	
	
	
	
	 
}
