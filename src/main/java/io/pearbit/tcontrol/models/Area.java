package io.pearbit.tcontrol.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;
@Component
@Entity //declara como entidad
@Table(name = "Area") //nombre de tabla física
public class Area {
	
	@Id //id
	@GeneratedValue //auto_increment
	@Column(name = "a_id")
	private int idArea;
	// idSubsidiary;
	@Column (name = "a_name")
	private String name;
	@Column (name = "a_desc")
	private String description;
	
	
    @OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="e_id")
	private List<Employee> employees;
	
	
	public Area(int idArea, String name, String description, List<Employee> employees) {
		super();
		this.idArea = idArea;
		this.name = name;
		this.description = description;
		this.employees = employees;
	}


	public Area() {
		super();
		// TODO Auto-generated constructor stub
	}


	public int getIdArea() {
		return idArea;
	}


	public void setIdArea(int idArea) {
		this.idArea = idArea;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public List<Employee> getEmployees() {
		return employees;
	}


	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}


	@Override
	public String toString() {
		return "Area [idArea=" + idArea + ", name=" + name + ", description=" + description + "]";
	}
	
	
	
	
	
	
}
