package io.pearbit.tcontrol.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.pearbit.tcontrol.dao.UserDao;
import io.pearbit.tcontrol.models.Rol;
import io.pearbit.tcontrol.models.User;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Custom User Detail Service, to made a custom authentication process. this can be replaced by an interface in a near future
 * @author JuanBG
 * @see UserDetailsService
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService{
    @Autowired
    private UserDao userRepository;

    
    
    /**
     * This method perform the login process
     * @param username user name 
     * @return the user details if all is okay, return  null if something was wrong and spring handle these null like an {@link AuthenticationCredentialsNotFoundException}}
     */
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws AuthenticationCredentialsNotFoundException {
        System.out.println("username" + username);
        Optional<User> user = null;
        UserDetails userDetails;
        user = userRepository.findByUsername(username);
		
        if(user.isPresent()) { // If user is present 
			Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		            grantedAuthorities.add(new SimpleGrantedAuthority(user.get().getRol().getName()));
		        userDetails =  new org.springframework.security.core.userdetails.User(user.get().getUsername(), user.get().getPassword(), grantedAuthorities);
			}else { // if not throw exception spring will handle it 
				throw new AuthenticationCredentialsNotFoundException("User not found"); 
			}
		

           return userDetails;
    }
}
